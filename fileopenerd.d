/*
Copyright 2020 Simon Arneaud

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import std.algorithm.mutation : swap;
import std.file;
import logger = std.experimental.logger;
import std.string;

import core.stdc.errno;
import core.stdc.string;
import core.sys.posix.fcntl;
import core.sys.posix.signal;
import core.sys.posix.unistd;

int main()
{
	// The loop suspends on each interation, to be woken up on SIGHUP
	sigset_t reload_sig;
	sigemptyset(&reload_sig);
	sigaddset(&reload_sig, SIGHUP);
	sigprocmask(SIG_BLOCK, &reload_sig, null);

	// For each iteration of the loop, open_fds is used to track file descriptors to keep open
	// while old_fds tracks the FDs from the previous iteration (that are to be closed by the end of the iteration)
	int[] open_fds, old_fds;

	const my_pid = getpid();
	logger.infof("Running as process ID %d", my_pid);

	while (true)
	{
		logger.infof("Opening files in directory `%s`", getcwd());

		swap(open_fds, old_fds);
		open_fds.reserve(old_fds.length);
		foreach (filename; dirEntries("", SpanMode.shallow, false))
		{
			logger.infof("Found file `%s`", filename);
			int fd = open(filename.toStringz, O_RDWR);
			if (fd < 0)
			{
				auto error_msg = strerror(errno).fromStringz();
				logger.errorf("Failed to open `%s`: %s", filename, error_msg);
			}
			else
			{
				logger.infof("File descriptor %d used for file `%s`", fd, filename);
				open_fds ~= fd;
			}
		}

		foreach (fd; old_fds)
		{
			logger.infof("Closing file descriptor %d", fd);
			close(fd);
		}
		old_fds.length = 0;

		logger.infof("Sleeping. To update open files: kill -HUP %d", my_pid);
		int sig_num;
		// Sleep here to wake up on SIGHUP
		int err_num = sigwait(&reload_sig, &sig_num);
		if (err_num)
		{
			auto error_msg = strerror(errno).fromStringz();
			logger.errorf("Error while waiting: %s", error_msg);
			return 1;
		}
		logger.info("Updating open files");
	}
	assert (false);
}
