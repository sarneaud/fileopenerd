## `fileopnerd`

`fileopnerd` opens a list of files, and keeps them open.  [It does not read email.](http://www.catb.org/jargon/html/Z/Zawinskis-Law.html)  The original purpose was [keeping named pipes open for logging use](https://theartofmachinery.com/2020/10/10/logging_with_named_pipes.html).

More specifically, `fileopenerd` opens a read/write file descriptor to every file in its current working directory and suspends.  On `SIGHUP`, it loads new file descriptors to every file in its working directory, closes all the old descriptors, then suspends again waiting for `SIGHUP`.

### Compiling, installing and running

It's a single D file, with no dependencies, and that should work on any POSIX-y system.  If you don't have a D compiler, there's a good chance `ldc2` or `gdc` are in whatever package manager you have.  (Otherwise, you can [download a D compiler directly](https://dlang.org/download.html).)

```
$ dmd fileopenerd.d
```

`ldc2` and `gdc` both support cross compilation.  If you can't find a good guide for your system, [ask on the D forum](https://forum.dlang.org/group/learn).

After compiling, just put the `fileopenerd` binary in an appropriate place for programs (e.g., `/usr/sbin/`).  Run it from a directory containing files to open.  It's convenient to use a dedicated directory (e.g., `/var/run/fileopenerd/`) filled with symbolic links to files (named pipes) that you want kept open.

### If it's meant to be a daemon, why doesn't it fork or write a PID file?

[Jonathan de Boyne Pollard explains why not here](https://jdebp.eu/FGA/unix-daemon-design-mistakes-to-avoid.html).  Historically, "features" like that were popular for daemons because of the way old init scripts work, but they caused problems and better systems have been around for decades now.  (I personally like [s6](http://www.skarnet.org/software/s6/index.html).)  If you *really* want that functionality, use a tool like [`daemonize`](https://linux.die.net/man/1/daemonize).
